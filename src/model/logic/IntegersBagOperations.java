package model.logic;

import java.util.Iterator;

import model.data_structures.IntegersBag;

public class IntegersBagOperations {

	
	
	public double computeMean(IntegersBag bag){
		double mean = 0;
		int length = 0;
		if(bag != null){
			Iterator<Integer> iter = bag.getIterator();
			while(iter.hasNext()){
				mean += iter.next();
				length++;
			}
			if( length > 0) mean = mean / length;
		}
		return mean;
	}
	
	
	public int getMax(IntegersBag bag){
		int max = Integer.MIN_VALUE;
	    int value;
		if(bag != null){
			Iterator<Integer> iter = bag.getIterator();
			while(iter.hasNext()){
				value = iter.next();
				if( max < value){
					max = value;
				}
			}
			
		}
		return max;
	}

	public int getMin(IntegersBag bag) {
		int min = Integer.MAX_VALUE;
	    int value;
		if(bag != null){
			Iterator<Integer> iter = bag.getIterator();
			while(iter.hasNext()){
				value = iter.next();
				if( min > value){
					min = value;
				}
			}
			
		}
		return min;
		
	}

	public double variance(IntegersBag bag) {
	    double mean = computeMean(bag);
        double temp = 0;
        int size =0;
        if(bag!=null) {
        	Iterator<Integer> iter = bag.getIterator();
			while(iter.hasNext()){
				temp += (iter.next()-mean)*(iter.next()-mean);
				size++;
				
			}
        }
        
            
        return  size == 0 ? 0:(temp/(size));
		
	}
	public double standarDeviation(IntegersBag bag) {
		return Math.sqrt(variance(bag));
	}
	
}
